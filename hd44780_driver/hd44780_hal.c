#include "hd44780_hal.h"
#include "stm32f10x.h"

#define bit(n)							(1u << (n))
#define setBit(x,n)           			((x) |= bit(n))
#define clrBit(x,n)  		  			((x) &= ~bit(n))
#define getBit(x,n)   		  			(((x) & bit(n)) ? 1u : 0u)

void LcdHal_GpioInit()
{
	RCC_APB2PeriphClockCmd (
			LCD_HAL_BUS_RCC	|
			LCD_HAL_CTRL_E_RCC |
			LCD_HAL_CTRL_RW_RCC |
			LCD_HAL_CTRL_RS_RCC,
			ENABLE);

	GPIO_InitTypeDef gpio;

	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;

	gpio.GPIO_Pin = LCD_HAL_BUS_D4 | LCD_HAL_BUS_D5 | LCD_HAL_BUS_D6 | LCD_HAL_BUS_D7;
	GPIO_Init(LCD_HAL_BUS_GPIO, &gpio);

	gpio.GPIO_Pin = LCD_HAL_CTRL_E_Pin;
	GPIO_Init(LCD_HAL_CTRL_E_GPIO, &gpio);

	gpio.GPIO_Pin = LCD_HAL_CTRL_RW_Pin;
	GPIO_Init(LCD_HAL_CTRL_RW_GPIO, &gpio);

	gpio.GPIO_Pin = LCD_HAL_CTRL_RS_Pin;
	GPIO_Init(LCD_HAL_CTRL_RS_GPIO, &gpio);
}

void LcdHal_SetBusAsOutput()
{
	GPIO_InitTypeDef gpio;

	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = LCD_HAL_BUS_D4 | LCD_HAL_BUS_D5 | LCD_HAL_BUS_D6 | LCD_HAL_BUS_D7;
	GPIO_Init(LCD_HAL_BUS_GPIO, &gpio);
}

void LcdHal_SetBusAsInput()
{
	GPIO_InitTypeDef gpio;

	gpio.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = LCD_HAL_BUS_D4 | LCD_HAL_BUS_D5 | LCD_HAL_BUS_D6 | LCD_HAL_BUS_D7;
	GPIO_Init(LCD_HAL_BUS_GPIO, &gpio);
}

void LcdHal_SetD7AsOutput()
{
	GPIO_InitTypeDef gpio;

	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin =  LCD_HAL_BUS_D7;
	GPIO_Init(LCD_HAL_BUS_GPIO, &gpio);
}

void LcdHal_SetD7AsInput()
{
	GPIO_InitTypeDef gpio;

	gpio.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin =  LCD_HAL_BUS_D7;
	GPIO_Init(LCD_HAL_BUS_GPIO, &gpio);
}

void LcdHal_WriteBus(uint8_t data)
{
	if(data & 0b1000)	{
		GPIO_SetBits(LCD_HAL_BUS_GPIO, LCD_HAL_BUS_D7);
	}
	else	{
		GPIO_ResetBits(LCD_HAL_BUS_GPIO, LCD_HAL_BUS_D7);
	}

	if(data & 0b0100)	{
		GPIO_SetBits(LCD_HAL_BUS_GPIO, LCD_HAL_BUS_D6);
	}
	else	{
		GPIO_ResetBits(LCD_HAL_BUS_GPIO, LCD_HAL_BUS_D6);
	}

	if(data & 0b0010)	{
		GPIO_SetBits(LCD_HAL_BUS_GPIO, LCD_HAL_BUS_D5);
	}
	else	{
		GPIO_ResetBits(LCD_HAL_BUS_GPIO, LCD_HAL_BUS_D5);
	}

	if(data & 0b0001)	{
		GPIO_SetBits(LCD_HAL_BUS_GPIO, LCD_HAL_BUS_D4);
	}
	else	{
		GPIO_ResetBits(LCD_HAL_BUS_GPIO, LCD_HAL_BUS_D4);
	}
}

uint8_t LcdHal_ReadBus()
{
	uint8_t data = 0;

	if ((LCD_HAL_BUS_GPIO->IDR & LCD_HAL_BUS_D4) != 0) {
		setBit(data, 0);
	}
	if ((LCD_HAL_BUS_GPIO->IDR & LCD_HAL_BUS_D5) != 0) {
		setBit(data, 1);
	}
	if ((LCD_HAL_BUS_GPIO->IDR & LCD_HAL_BUS_D6) != 0) {
		setBit(data, 2);
	}
	if ((LCD_HAL_BUS_GPIO->IDR & LCD_HAL_BUS_D7) != 0) {
		setBit(data, 3);
	}
	return data;
}

bool LcdHal_ReadD7()
{
	if ((LCD_HAL_BUS_GPIO->IDR & LCD_HAL_BUS_D7) != 0) {
		return true;
	}
	else {
		return false;
	}
}

void LcdHal_SetE()
{
	LCD_HAL_CTRL_E_GPIO->BSRR = LCD_HAL_CTRL_E_Pin;
}

void LcdHal_ResetE()
{
	LCD_HAL_CTRL_E_GPIO->BRR = LCD_HAL_CTRL_E_Pin;
}

void LcdHal_SetRW()
{
	LCD_HAL_CTRL_RW_GPIO->BSRR = LCD_HAL_CTRL_RW_Pin;
}

void LcdHal_ResetRW()
{
	LCD_HAL_CTRL_RW_GPIO->BRR = LCD_HAL_CTRL_RW_Pin;
}

void LcdHal_SetRS()
{
	LCD_HAL_CTRL_RS_GPIO->BSRR = LCD_HAL_CTRL_RS_Pin;
}

void LcdHal_ResetRS()
{
	LCD_HAL_CTRL_RS_GPIO->BRR = LCD_HAL_CTRL_RS_Pin;
}
