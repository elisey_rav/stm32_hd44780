#pragma once

#include "stm32f10x.h"
#include <stdint.h>
#include <stdbool.h>

#define LCD_HAL_BUS_RCC			RCC_APB2Periph_GPIOA
#define LCD_HAL_CTRL_E_RCC		RCC_APB2Periph_GPIOC
#define LCD_HAL_CTRL_RW_RCC		RCC_APB2Periph_GPIOC
#define LCD_HAL_CTRL_RS_RCC		RCC_APB2Periph_GPIOC

#define LCD_HAL_BUS_GPIO		GPIOA
#define LCD_HAL_BUS_D4			GPIO_Pin_0
#define LCD_HAL_BUS_D5			GPIO_Pin_1
#define LCD_HAL_BUS_D6			GPIO_Pin_2
#define LCD_HAL_BUS_D7			GPIO_Pin_3

#define LCD_HAL_CTRL_E_GPIO		GPIOA
#define LCD_HAL_CTRL_E_Pin		GPIO_Pin_5

#define LCD_HAL_CTRL_RW_GPIO	GPIOA
#define LCD_HAL_CTRL_RW_Pin		GPIO_Pin_6

#define LCD_HAL_CTRL_RS_GPIO	GPIOA
#define LCD_HAL_CTRL_RS_Pin		GPIO_Pin_7

void LcdHal_GpioInit();
void LcdHal_SetBusAsOutput();
void LcdHal_SetBusAsInput();
void LcdHal_SetD7AsOutput();
void LcdHal_SetD7AsInput();

void LcdHal_WriteBus(uint8_t data);
uint8_t LcdHal_ReadBus();
bool LcdHal_ReadD7();

void LcdHal_SetE();
void LcdHal_ResetE();
void LcdHal_SetRW();
void LcdHal_ResetRW();
void LcdHal_SetRS();
void LcdHal_ResetRS();
